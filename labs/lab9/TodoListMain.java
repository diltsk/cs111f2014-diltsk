//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Lab #9
// Date: 13 November 2014
//
// Purpose:To make a todo list
//****************************************************************

import java.util.ArrayList; //Allows use of array lists
import java.util.Iterator;  //Allows use of iterators
import java.io.IOException;
import java.util.Scanner;   //Allows user input

public class TodoListMain {

    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to the Todo List Manager!"); //Print greeting and instructions
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, done, list, quit");

        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList(); //Creates todoList method

        while(scanner.hasNext()) {              //Begins while loop
            String command = scanner.nextLine();
            if(command.equals("read")) {
                todoList.readTodoItemsFromFile();
            }
            else if(command.equals("list")) {   //Command "list" reaction
                System.out.println(todoList.toString());
            }
            else if(command.equals("done")) {   //Command "done" reaction
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsDone(chosenId);
            }
            else if(command.equals("category-search")) { //Command "category-search" reaction
                System.out.println("What category would you like to search?");//Question for user
                String chosenCat = scanner.nextLine();
                Iterator iterator = todoList.findTasksOfCategory(chosenCat); //Gets iterator from todoList.java
                while(iterator.hasNext())   {
                    System.out.println(iterator.next()); //Prints requested objects through the iterator
                }
            }
            else if(command.equals("priority-search")) { //Command "priority-search" reaction
                System.out.println("What priority of item would you like to search?");//Question for user
                String chosenPri = scanner.nextLine();
                Iterator iterator = todoList.findTasksOfPriority(chosenPri); //Gets iterator from todoList.java
                while(iterator.hasNext())   {
                    System.out.println(iterator.next()); //Prints requested objects through the iterator
                }
            }
            else if(command.equals("quit")) {
                break;
            }
        }

    }


}

