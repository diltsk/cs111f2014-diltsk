import java.util.ArrayList; //Allows use of array lists
import java.util.Iterator;  //Allows use of iterators
import java.util.Scanner;   //Allows user input
import java.io.File;
import java.io.IOException;

public class TodoList {

    private ArrayList<TodoItem> todoItems;
    private static final String TODOFILE = "todo.txt"; //Pull info from "todo.txt"

    public TodoList() {
        todoItems = new ArrayList<TodoItem>(); //Creates an array list out of "TodoItem"
    }

    public void addTodoItem(TodoItem todoItem) {
        todoItems.add(todoItem);
    }

    public Iterator getTodoItems() {
        return todoItems.iterator(); //Returns an iterator of todoItems
    }

    public void readTodoItemsFromFile() throws IOException {   //Establishes possible user input commands
        Scanner fileScanner = new Scanner(new File(TODOFILE));
        while(fileScanner.hasNext()) {
            String todoItemLine = fileScanner.nextLine();
            Scanner todoScanner = new Scanner(todoItemLine);
            todoScanner.useDelimiter(",");
            String priority, category, task;
            priority = todoScanner.next();
            category = todoScanner.next();
            task = todoScanner.next();
            TodoItem todoItem = new TodoItem(priority, category, task);
            todoItems.add(todoItem);
        }
    }

    public void markTaskAsDone(int toMarkId {  //Marks tasks in todoList as done
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getId() == toMarkId) {
                todoItem.markDone();
            }
        }
    }

    public Iterator findTasksOfPriority(String requestedPriority) { //Allows user to request the priority of a task
        ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>();
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            TodoItem todoItem = (TodoItem)iterator.next();
            if (todoItem.getPriority().equals(requestedPriority))  {
                priorityList.add(todoItem);
            }
        }
        return priorityList.iterator(); //Returns an iterator
    }

    public Iterator findTasksOfCategory(String requestedCategory) { //Allows user to request the category of a task
        ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>();
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) { //Creates while loop for iterator
            TodoItem todoItem = (TodoItem)iterator.next();
            if (todoItem.getCategory().equals(requestedCategory)) {
                categoryList.add(todoItem);
            }
        }
        return categoryList.iterator(); //Returns an iterator
    }


    public String toString() {  //Returns the full todoItem
        StringBuffer buffer = new StringBuffer();
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            buffer.append(iterator.next().toString());
            if(iterator.hasNext()) {
                buffer.append("\n");
            }
        }
        return buffer.toString();
    }

}

