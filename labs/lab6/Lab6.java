//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Lab #6
// Date:9 Oct 2014
//
// Purpose:To create a program that can determine various volumes/areas of different objects
//****************************************************************
import java.util.Date; //Needed for today's date
import java.util.Scanner; //Need to use Scanner feature
import java.text.DecimalFormat; //Needed to limit decimal places

public class Lab6
{

    private enum GeometricShape {sphere, triangle, cylinder};

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        GeometricShape shape = GeometricShape.sphere;
        //Variable library
        double radius;
        double height;
        double x;
        double y;
        double z;

        //Limits decimals to 4 places
        DecimalFormat fmt = new DecimalFormat("0.####");


        //Prints heading/greeting
        System.out.println("Kyle Dilts " + new Date());
        System.out.println("Welcome to the Lab6 Volume/Surface Area Program!");
        System.out.println();

        //Ask user for radius of sphere
        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();

        //Call class to calculate volume of sphere
        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double sphereVolume = GeometricCalculator.calculateSphereVolume(radius);
        //Print results
        System.out.println("The volume is equal to " + fmt.format(sphereVolume));
        System.out.println();

        shape = GeometricShape.triangle; //Designate new shape variable

        //Ask user for sides of triangle
        System.out.println("What is the length of the first side?");
        x = scan.nextDouble();

        System.out.println("What is the length of the second side?");
        y = scan.nextDouble();

        System.out.println("What is the length of the third side?");
        z = scan.nextDouble();
        System.out.println();

        //Call method to calculate triangle area
        System.out.println("Calculating the area of a " + shape);
        double triangleArea = GeometricCalculator.calculateTriangleArea(x, y, z);
        //Print Results
        System.out.println("The area is equal to " + fmt.format(triangleArea));
        System.out.println();

        shape = GeometricShape.cylinder; //Designate new shape variable

        //Ask user for cylinder radius
        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();
        //Ask user for cylinder height
        System.out.println("What is the height for the " + shape + "?");
        height = scan.nextDouble();
        System.out.println();

        //Call class to calculate the volume of the cylinder
        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double cylinderVolume = GeometricCalculator.calculateCylinderVolume(radius, height);
        //Print Results
        System.out.println("The volume is equal to " + fmt.format(cylinderVolume));
        System.out.println();

        shape = GeometricShape.cylinder; //Designate new shape variable

        //Ask user for radius of the cylinder
          System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();

        //Ask user for height of the cylinder
        System.out.println("What is the height for the " + shape + "?");
        height = scan.nextDouble();
        System.out.println();

        //Display Results
        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double cylinderSurfacearea = GeometricCalculator.calculateCylinderSurfacearea(radius, height);
        System.out.println("The surface area is equal to " + fmt.format(cylinderSurfacearea));
        System.out.println();

    }
}
