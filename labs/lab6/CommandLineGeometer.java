//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Lab #6
// Date:9 Oct 2014
//
// Purpose:
//****************************************************************
import java.util.Date; //Needed for today's date
import java.util.Scanner; //Need to use Scanner feature
import java.text.DecimalFormat; //Needed to limit decimal places

public class Lab6
{

    private enum GeometricShape {sphere, triangle, cylinder};

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        GeometricShape shape = GeometricShape.sphere;
        double radius;
        double height;
        int x;
        int y;
        int z;

        System.out.println("Kyle Dilts " + new Date());
        System.out.println("Welcome to the Command Line Geometer!");
        System.out.println();

        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();

        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double sphereVolume = GeometricCalculator.calculateSphereVolume(radius);
        System.out.println("The volume is equal to " + sphereVolume);
        System.out.println();

        shape = GeometricShape.triangle;

        System.out.println("What is the length of the first side?");
        x = scan.nextInt();

        System.out.println("What is the length of the second side?");
        y = scan.nextInt();

        System.out.println("What is the length of the third side?");
        z = scan.nextInt();
        System.out.println();

        System.out.println("Calculating the area of a " + shape);
        double triangleArea = GeometricCalculator.calculateTriangleArea(x, y, z);
        System.out.println("The area is equal to " + triangleArea);
        System.out.println();

        shape = GeometricShape.cylinder;

        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();

        System.out.println("What is the height for the " + shape + "?");
        height = scan.nextDouble();
        System.out.println();

        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double cylinderVolume = GeometricCalculator.calculateCylinderVolume(radius, height);
        System.out.println("The volume is equal to " +  cylinderVolume);
        System.out.println();

         System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();

        System.out.println("What is the height for the " + shape + "?");
        height = scan.nextDouble();
        System.out.println();

        System.out.println("Calculation the surface area of a " + shape + " with a radius equal; to " + radius);
        double cylinderSurfacearea = GeometricCalculator.calculateCylinderSurfacearea(radius, height);
        System.out.println("The surface area is equal to " cylinderSurfacearea);
        System.out.println();
    }
}
