//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Lab #6
// Date:9 Oct 2014
//
// Purpose:To create a program that can determine various volumes/areas of different objects
//****************************************************************


import java.lang.Math;//Allows for use of math functions

public class GeometricCalculator {

    // create class for sphere
    public static double calculateSphereVolume(double radius) {
        double volume;
        volume = (4/3) * (Math.PI) * (radius * radius * radius);
        return volume;
    }
    // create class for triangle
    public static double calculateTriangleArea(double a, double b, double c) {
        double area;
        double s;
        s = (a + b + c) / 2;
        area = (s * (s - a) * (s - b) * (s - c));
        area = Math.sqrt(area);
        return area;
    }
    // create class for volume of a cylinder
    public static double calculateCylinderVolume(double radius, double height) {
        double volume;
        volume = (Math.PI) * (radius * radius) * height;
        return volume;
    }
    // create class for surface area of a cylinder
    public static double calculateCylinderSurfacearea(double radius, double height) {
        double Surfacearea;
        Surfacearea = 2 * (Math.PI) * radius * height;
        return Surfacearea;
    }
}

