
//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Lab #
// Date:
//
// Purpose: To find the average Population of intelligent people per Square Mile of the United States
//****************************************************************
import java.util.Date; //Needed for today's date

public class Lab2
{
    //-------------------------------------------
    // main method: program execution begins here
    //-------------------------------------------
    public static void main(String[] args)
    {
       // Label output with name and date:
       System.out.println("Kyle Dilts\nLab #2\n" + new Date() + "\n");

       // Variable dictionary:
	long PopofNorthEastUS = 55317240;   //Approx. Population of the Northeast US in 2010
	long PopofSthrnUS = 114555744;      //Approx. Population of Southern US in 2010
	long PopofWestUS = 71945553;	    //Approx. Population of Western US in 2010
	long PopofMidWestUS = 66927001;     //Approx. Population of the Midwestern US in 2010
	long SqMilesofUS = 3794083;         //Square Miles of the US
	long PopofUS;	                    //Population of US in 2010
	long IntPopofUS;		    //Intelligent Population of the US in 2010
	long IntPplPerSquareMile;           //Intelligent People per Square Mile

	// Find US Population in 2010
	PopofUS = PopofNorthEastUS + PopofSthrnUS + PopofWestUS + PopofMidWestUS;
	// Subtract Unintelligent People
	IntPopofUS = PopofUS - PopofSthrnUS;
	// Find Intelligent US Population per Square Mile
	IntPplPerSquareMile = IntPopofUS / SqMilesofUS;
	// Print Data

	System.out.println("Population of the US in 2010: " + PopofUS);
	System.out.println("Intelligent Population of the US in 2010: " + IntPopofUS);
	System.out.println("Square Miles of the US: " + SqMilesofUS);
	System.out.println("Intelligent People per Square Mile in 2010: " + IntPplPerSquareMile);
    }
}
