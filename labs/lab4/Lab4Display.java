//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Lab #4 
// Date:25 September 2014
//
// Purpose:To Draw a Flower
//****************************************************************
import java.util.Date; //Needed for today's date
import javax.swing.*;

public class Lab4Display
{
    public static void main(String[] args)
    {
        	JFrame window = new JFrame(" Flower ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new Lab4());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
			window.setSize(600, 400);
        	
    }
}

