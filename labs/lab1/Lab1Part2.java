//*******************************
// Kyle Dilts
// CMPSC111
// 4 September 2014
// Lab1Part2
//
// 
//
//*******************************

import java.util.Date;

public class Lab1Part2
{
	public static void main(String[] args)
	{
	 System.out.println("Kyle Dilts " + new Date());
	 System.out.println("Lab 1Part2");
	
	//-----------------------------------------
	// Print Do You Hear the People Sing? and
	// then move each line down one
	//-----------------------------------------
	  System.out.println("Do you hear the people sing?\n" + 
		"Singing a song of angry men?\n" +
		"It is the music of a people,\n" + 
		"Who will not be slaves again!\n" +
		"When the beating of your heart,\n" +
		"Echoes the beating of a drum,\n" + 
		"There is a life about to start as tomorrow comes!\n");

	}
}
