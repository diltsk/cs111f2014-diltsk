//*****************************************
// Addition.java       Author Lewis/Loftus
//
// Demonstrates the difference between the addition and string
// concatenation operators.
//*****************************************

public class Addition
{
   //-----------------------------------------------------------
   // Concatenates and adds two numbers and prints the results.
   //-----------------------------------------------------------
   public static void main(String[] args)
   {
    System.out.println("24 and 45 concatenated: " + 24 + 45);

    System.out.println("24 and 45 added " + (24 + 45));
    }
}

/* The first print line makes 24 and 45 print without anything between them
(glued together) and the second uses uses an additional set of parenthesis which adds the two numbers together*/
