//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Lab #7
// Date:10 October 2014
//
// Purpose:To Draw a Flower
//****************************************************************
import java.util.Date; //Needed for today's date
import java.awt.*;
import javax.swing.JApplet;

public class Lab7Drawing extends JApplet
{
    //-----------------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-----------------------------------------------------------

	//Define Int Variable
	final int Grassline = 300;

	//Define colors
    Color stemgreen = new Color(42, 156, 0);

    public void Background(Graphics page)//Creates a Method for the Background
    {
        setBackground(Color.cyan); //Color the sky
        page.setColor(Color.green); //Grass
        page.fillRect(0, Grassline, 600, 175);
    }

    public void Sun(Graphics page)//Creates a Method for the Sun
    {
	page.setColor(Color.yellow); //Sun
    page.fillOval(-40, -40, 80, 80);

	page.setColor(Color.green); //Grass
	page.fillRect(0, Grassline, 600, 175);

    page.setColor(Color.yellow);//Sun's rays
    page.drawLine(50, 70, 90, 110);
    page.drawLine(50, 60, 100, 90);
    page.drawLine(70, 60, 100, 80);
    }

    public void Flower(Graphics page) //Creates a method for the Flower
    {
	page.setColor(stemgreen); //Stem
	page.fillRect(Grassline, Grassline - 150, 35, 150);

    page.setColor(stemgreen); //leaves
	page.fillOval(Grassline, 190, 75, 50);
	page.fillOval(Grassline - 45, 190, 75, 50);

	page.setColor(Color.yellow);//Petals
	page.fillOval(Grassline, 117, 75, 50);
	page.fillOval(Grassline - 40, 117, 75, 50);
	page.fillOval(Grassline - 9, 100, 55, 85);

	page.setColor(Color.black); //Center of Flower
	page.fillOval(Grassline - 7, Grassline-180, 50, 50);
    }

}
