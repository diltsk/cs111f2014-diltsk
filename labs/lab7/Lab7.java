//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Lab #7
// Date:10 October 2014
//
// Purpose:To Draw a Flower
//****************************************************************
import java.util.Date; //Needed for today's date
import java.awt.*;
import javax.swing.JApplet;

public class Lab7 extends JApplet
{

   public void paint(Graphics page)
   {

    Lab7Drawing Picture = new Lab7Drawing(); //Create a Constructor

    Picture.Background(page); //Call Background Method
    Picture.Sun(page);        //Call Background Method
    Picture.Flower(page);     //Call Background Method

   }
}
