//**************************************************
// Honor Code: The work I am submitting is a result of my
// own thinking and efforts
//
// Sam Rodman & Kyle Dilts
// CMPSC 111 Fall 2014
// Lab #5
// Date: October 2
//
// Purpose: To create a DNA string modifier
//***************************************************
import java.util.Date;
import java.util.Scanner;
import java.util.Random;

public class Lab5
{
    public static void main(String[] args)
    {
        System.out.println("Sam Rodman & Kyle Dilts\nLab #5\n" + new Date() + "\n");
        Scanner scan = new Scanner(System.in);// Allows user input
        Random r = new Random(); //Random number generation

        //Variable Library
        String dnaString;//Variable for input
        int len = 15;//Length of DNA strand
        String s2;//Creates building block for complimentary strand
        int Deleted;//Creates building block for deletion of a piece of a strand
        String sub1, sub2, sub3, sub4;//Building blocks for substitution
        int Deleted2;//Building block for second deletion

        System.out.print("Please input 16 characters using only ");
        System.out.println("the characters A, T, G, and C: ");

        dnaString = scan.next(); //Store character input for DNA

        s2 = dnaString; //Building block of Complimentary strand
        //Make all characters lower case            <`0`>
        s2 = s2.toLowerCase();

        //Creating complimentary string
        s2 = s2.replace('a','T');
        s2 = s2.replace('t','A');
        s2 = s2.replace('c','G');
        s2 = s2.replace('g','C');

        System.out.println("Your original string was " + dnaString);//Print original strand
        System.out.println("The complimentary strand is " + s2);//Print complimentary strand

        char mutation1 = "ATCG".charAt(r.nextInt(4)); //Changes one letter
        System.out.println("Insert " + mutation1 +" at position 0: " + mutation1 + dnaString);

        //Deleting one character randomly
        Deleted = r.nextInt(16);
        System.out.print("Deleting from position " + Deleted + ": ");

        sub1 = dnaString.substring(0,Deleted);
        sub2 = dnaString.substring(Deleted + 1, 16);

        System.out.println(sub1 + sub2);

        //Changing position of one character
        Deleted2 = r.nextInt(16);
        System.out.print("Now we change position " + Deleted2 + ": ");

        //Deletes and replaces one character
        sub3 = dnaString.substring(0, Deleted2);
        sub4 = dnaString.substring(Deleted2 + 1, 16);
        char mutation2 = "ATGC".charAt(r.nextInt(4));
        System.out.println(sub3 + mutation2 + sub4);

    }
}


