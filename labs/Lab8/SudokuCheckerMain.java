//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Lab #8
// Date:30 Oct 2014
//
// Purpose:To check a 4x4 Sudoku Grid
//****************************************************************
import java.util.Date; //Needed for today's date
import java.util.Scanner; //Need to use Scanner feature
import java.text.DecimalFormat; //Needed to limit decimal places

public class SudokuCheckerMain
{
    //-------------------------------------------
    // main method: program execution begins here
    //-------------------------------------------
    public static void main(String[] args)
    {
       // Label output with name and date:
       System.out.println("Kyle Dilts\nLab #8\n" + new Date() + "\n");


    // Print Welcome Message
        System.out.println("Welcome to the Sudoku Checker!");
        System.out.println("This program checks 4x4 Sudoku grids for correctness.");
        System.out.println("Each column, row, and region contains the numbers 1 through 4 only once.");
        System.out.println("To check your grid, enter your board one row at at time.");
        System.out.println("Remember to leave a space between each digit!");
        System.out.println("Please press ENTER at the end of each row.");

       SudokuChecker checker = new SudokuChecker();

       //Call methods
       checker.getGrid();
       checker.checkGrid();




    }
}
