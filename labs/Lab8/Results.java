
Kyle Dilts
Lab #8
Thu Nov 06 13:42:01 EST 2014

Welcome to the Sudoku Checker!
This program checks 4x4 Sudoku grids for correctness.
Each column, row, and region contains the numbers 1 through 4 only once.
To check your grid, enter your board one row at at time.
Remember to leave a space between each digit!
Please press ENTER at the end of each row.
Enter row 1: 3 2 4 1
Enter row 2: 4 1 4 2
Enter row 3: 1 4 2 3
Enter row 4: 2 3 1 4
Row 1: Good
Row 2: Bad
Row 3: Good
Row 4: Good
Region 1: Good
Region 2: Good
Region 3: Bad
Region 4: Good
Column 1: Good
Column 2: Good
Column 3: Bad
Column 4: Good
This Sudoku Grid is Wrong.
diltsk@aldenv178:~/cs111F2014/cs111F2014-diltsk/labs/Lab8$ java SudokuCheckerMain
Kyle Dilts
Lab #8
Thu Nov 06 13:43:22 EST 2014

Welcome to the Sudoku Checker!
This program checks 4x4 Sudoku grids for correctness.
Each column, row, and region contains the numbers 1 through 4 only once.
To check your grid, enter your board one row at at time.
Remember to leave a space between each digit!
Please press ENTER at the end of each row.
Enter row 1: 3 2 4 1
Enter row 2: 4 1 3 2
Enter row 3: 1 4 2 3
Enter row 4: 2 3 1 4
Row 1: Good
Row 2: Good
Row 3: Good
Row 4: Good
Region 1: Good
Region 2: Good
Region 3: Good
Region 4: Good
Column 1: Good
Column 2: Good
Column 3: Good
Column 4: Good
This Sudoku Grid is Wrong.
diltsk@aldenv178:~/cs111F2014/cs111F2014-diltsk/labs/Lab8$ javac *.java
diltsk@aldenv178:~/cs111F2014/cs111F2014-diltsk/labs/Lab8$ java SudokuCheckerMain
Kyle Dilts
Lab #8
Thu Nov 06 14:00:37 EST 2014

Welcome to the Sudoku Checker!
This program checks 4x4 Sudoku grids for correctness.
Each column, row, and region contains the numbers 1 through 4 only once.
To check your grid, enter your board one row at at time.
Remember to leave a space between each digit!
Please press ENTER at the end of each row.
Enter row 1: 3 2 4 1
Enter row 2: 4 1 3 2
Enter row 3: 1 4 2 3
Enter row 4: 2 3 1 4
Row 1: Good
Row 2: Good
Row 3: Good
Row 4: Good
Region 1: Good
Region 2: Good
Region 3: Good
Region 4: Good
Column 1: Good
Column 2: Good
Column 3: Good
Column 4: Good
This Sudoku Grid is Right!
diltsk@aldenv178:~/cs111F2014/cs111F2014-diltsk/labs/Lab8$ java SudokuCheckerMain
Kyle Dilts
Lab #8
Thu Nov 06 14:00:51 EST 2014

Welcome to the Sudoku Checker!
This program checks 4x4 Sudoku grids for correctness.
Each column, row, and region contains the numbers 1 through 4 only once.
To check your grid, enter your board one row at at time.
Remember to leave a space between each digit!
Please press ENTER at the end of each row.
Enter row 1: 1 2 3 4
Enter row 2: 1 2 3 4
Enter row 3: 1 2 3 4
Enter row 4: 1 2 3 4
Row 1: Good
Row 2: Good
Row 3: Good
Row 4: Good
Region 1: Bad
Region 2: Bad
Region 3: Bad
Region 4: Bad
Column 1: Bad
Column 2: Bad
Column 3: Bad
Column 4: Bad
This Sudoku Grid is Wrong.
diltsk@aldenv178:~/cs111F2014/cs111F2014-diltsk/labs/Lab8$ java SudokuCheckerMain
Kyle Dilts
Lab #8
Thu Nov 06 14:01:40 EST 2014

Welcome to the Sudoku Checker!
This program checks 4x4 Sudoku grids for correctness.
Each column, row, and region contains the numbers 1 through 4 only once.
To check your grid, enter your board one row at at time.
Remember to leave a space between each digit!
Please press ENTER at the end of each row.
Enter row 1: 4 2 3 1
Enter row 2: 2 3 4 1
Enter row 3: 1 2 3 4
Enter row 4: 2 3 1 4
Row 1: Good
Row 2: Good
Row 3: Good
Row 4: Good
Region 1: Bad
Region 2: Bad
Region 3: Bad
Region 4: Bad
Column 1: Bad
Column 2: Good
Column 3: Bad
Column 4: Good
This Sudoku Grid is Wrong.
diltsk@aldenv178:~/cs111F2014/cs111F2014-diltsk/labs/Lab8$ java SudokuCheckerMain
Kyle Dilts
Lab #8
Thu Nov 06 14:01:53 EST 2014

Welcome to the Sudoku Checker!
This program checks 4x4 Sudoku grids for correctness.
Each column, row, and region contains the numbers 1 through 4 only once.
To check your grid, enter your board one row at at time.
Remember to leave a space between each digit!
Please press ENTER at the end of each row.
Enter row 1: 3 4 2 1 
Enter row 2: 2 3 4 1
Enter row 3: 2 3 4 1
Enter row 4: 2 4 2 1
Row 1: Good
Row 2: Good
Row 3: Good
Row 4: Bad
Region 1: Bad
Region 2: Bad
Region 3: Bad
Region 4: Bad
Column 1: Bad
Column 2: Bad
Column 3: Bad
Column 4: Bad
This Sudoku Grid is Wrong.
diltsk@aldenv178:~/cs111F2014/cs111F2014-diltsk/labs/Lab8$ 
diltsk@aldenv178:~/cs111F2014/cs111F2014-diltsk/labs/Lab8$ java SudokuCheckerMain
Kyle Dilts
Lab #8
Thu Nov 06 14:02:05 EST 2014

Welcome to the Sudoku Checker!
This program checks 4x4 Sudoku grids for correctness.
Each column, row, and region contains the numbers 1 through 4 only once.
To check your grid, enter your board one row at at time.
Remember to leave a space between each digit!
Please press ENTER at the end of each row.
Enter row 1: 1 2 3 4
Enter row 2: 3 4 2 1
Enter row 3: 4 3 2 1
Enter row 4: 2 4 3 1
Row 1: Good
Row 2: Good
Row 3: Good
Row 4: Good
Region 1: Good
Region 2: Bad
Region 3: Good
Region 4: Bad
Column 1: Good
Column 2: Bad
Column 3: Good
Column 4: Bad
This Sudoku Grid is Wrong.
