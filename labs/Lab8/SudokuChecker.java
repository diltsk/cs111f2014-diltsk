
//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Lab # 8
// Date: 30 October 2014
//
// Purpose: To create a sudoku checker
//****************************************************************
import java.util.Scanner; //Need to use Scanner feature


public class SudokuChecker

{
        Scanner scan = new Scanner(System.in); //Allow user input
    //-------------------------------------------
    // main method: program execution begins here
    //-------------------------------------------

    // Variable Library

        private int q1; //First row of numbers
        private int q2;
        private int q3;
        private int q4;

        private int w1; //Second row of numbers
        private int w2;
        private int w3;
        private int w4;

        private int e1; //Third row of numbers
        private int e2;
        private int e3;
        private int e4;

        private int r1; //Fourth row of numbers
        private int r2;
        private int r3;
        private int r4;

        int Wrong;

        public SudokuChecker() // Constructor
        {
        //Set variables to 0
        q1 = 0;
        q2 = 0;
        q3 = 0;
        q4 = 0;

        w1 = 0;
        w2 = 0;
        w3 = 0;
        w4 = 0;

        e1 = 0;
        e2 = 0;
        e3 = 0;
        e4 = 0;

        r1 = 0;
        r2 = 0;
        r3 = 0;
        r4 = 0;


        }


        public void getGrid() //Getting user input for the grid
        {

        System.out.print("Enter row 1: ");
         q1 = scan.nextInt(); //Scan first row
         q2 = scan.nextInt();
         q3 = scan.nextInt();
         q4 = scan.nextInt();

        System.out.print("Enter row 2: ");
         w1 = scan.nextInt(); //Scan second row
         w2 = scan.nextInt();
         w3 = scan.nextInt();
         w4 = scan.nextInt();

         System.out.print("Enter row 3: ");
         e1 = scan.nextInt(); //Scan third row
         e2 = scan.nextInt();
         e3 = scan.nextInt();
         e4 = scan.nextInt();

         System.out.print("Enter row 4: ");
         r1 = scan.nextInt(); //Scan fourth row
         r2 = scan.nextInt();
         r3 = scan.nextInt();
         r4 = scan.nextInt();

        }

        public void checkGrid() //Checking the grid using if else statements
        {
         if (q1 + q2 + q3 + q4 == 10)
             System.out.println("Row 1: Good");
         else
            {
             System.out.println("Row 1: Bad");
             Wrong++;
            }
         if (w1 + w2 + w3 + w4 == 10)
            System.out.println("Row 2: Good");
         else
            {
             System.out.println("Row 2: Bad");
             Wrong++;
            }
         if (e1 +  e2 + e3 + e4 == 10)
             System.out.println("Row 3: Good");
         else
            {
             System.out.println("Row 3:Bad");
             Wrong++;
            }
         if (r1 + r2 + r3 + r4 == 10)
             System.out.println("Row 4: Good");
         else
            {
             System.out.println("Row 4: Bad");
             Wrong++;
            }
         if (q1 + q2 + w1 + w2 == 10)
             System.out.println("Region 1: Good");
         else
             {
             System.out.println("Region 1: Bad");
             Wrong++;
             }
         if (e1 + e2 + r1 + r2 == 10)
             System.out.println("Region 2: Good");
         else
            {
             System.out.println("Region 2: Bad");
             Wrong++;
            }
         if (q3 + q4 + w3 + w4 == 10)
             System.out.println("Region 3: Good");
         else
            {
             System.out.println("Region 3: Bad");
             Wrong++;
            }
         if (e3 + e4 + r3 + r4 == 10)
             System.out.println("Region 4: Good");
         else
            {
             System.out.println("Region 4: Bad");
             Wrong++;
            }
         if (q1 + w1 + e1 + r1 == 10)
             System.out.println("Column 1: Good");
         else
            {
             System.out.println("Column 1: Bad");
             Wrong++;
            }
         if (q2 + w2 + e2 + r2 == 10)
             System.out.println("Column 2: Good");
         else
            {
             System.out.println("Column 2: Bad");
             Wrong++;
            }
         if (q3 + w3 + e3 + r3 == 10)
             System.out.println("Column 3: Good");
         else
            {
             System.out.println("Column 3: Bad");
             Wrong++;
            }
         if (q4 + w4 + e4 + r4 == 10)
             System.out.println("Column 4: Good");
         else
            {
             System.out.println("Column 4: Bad");
             Wrong++;
            }
         //Determine if the entire grid is correct using if else statement
         if (Wrong == 0)
             System.out.println("This Sudoku Grid is Right!");
         else
             System.out.println("This Sudoku Grid is Wrong.");

        }




}
