//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Lab #3
// Date: 18 Sept. 2014
//
// Purpose: To calulate the tip of a restaurant bill 
//****************************************************************
import java.util.Date; //Needed for today's date
import java.util.Scanner; //Need to use Scanner feature
import java.text.DecimalFormat; //Needed to limit decimal places

public class Lab3 
{
    //-------------------------------------------
    // main method: program execution begins here
    //-------------------------------------------
    public static void main(String[] args)
    {
       // Label output with name and date:
       System.out.println("Kyle Dilts\nLab #3\n" + new Date() + "\n");

       // Variable dictionary:
	String Name; //Get user's name for friendly message
	//Variables for tip calculation
	double Total_Bill;
	float Percentage;
	double Tip;
	double Bill;
	int Number_of_People;
	double AmtPaidbyEach;
	Scanner scan = new Scanner(System.in);

	// Ask user's name
	System.out.print("Please enter your name: ");
	Name = scan.next();

	//Display friendly welcome message
	System.out.println("Hello " + Name + "!");
	System.out.println("Welcome to the tip calculator.");

	//Retrieve current bill
	System.out.print("Please enter the current bill: ");
	Bill = scan.nextDouble();

	//Retrieve tip percentage
	System.out.print("Please enter wanted tip percentage as a decimal: ");
	Percentage = scan.nextFloat();
	
	//Retrieve number of people paying
	System.out.print("Please enter number of people paying: ");
	Number_of_People = scan.nextInt();
	
	//Calculate tip
	Tip = Percentage * Bill;
	//Calculate total bill
	Total_Bill = Bill + Tip;
	//Calculate amount paid by each person
	AmtPaidbyEach = Total_Bill / Number_of_People;
	
	//Set decimal forma
	DecimalFormat fmt = new DecimalFormat("0.##");

	//Display results
	System.out.println("Amount of original bill: " + fmt.format(Bill));
	System.out.println("Tip amount calculated: " + fmt.format(Tip));
	System.out.println("Total bill: " + fmt.format(Total_Bill));
	System.out.println("Amount paid by each person: " + fmt.format(AmtPaidbyEach));
	System.out.println("Have a nice day!");
	
    }
}
