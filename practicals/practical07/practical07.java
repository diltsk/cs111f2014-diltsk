//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Lab #
// Date:
//
// Purpose:
//****************************************************************
import java.util.Date; //Needed for today's date
import java.util.Scanner; //Need to use Scanner feature
import java.util.Random; //Needed for a Random Number

public class practical07
{
    //-------------------------------------------
    // main method: program execution begins here
    //-------------------------------------------
    public static void main(String[] args)
    {
    Scanner scan = new Scanner(System.in); //Allows user to input data
    Random r = new Random(); //Allows for a random number

       // Label output with name and date:
       System.out.println("Kyle Dilts\nLab #\n" + new Date() + "\n");

       // Variable dictionary:

        int input;
        int number;
        int tries;

        input = 0;
        tries = 0;

        //Welcome message
        System.out.println("Hi! Lets play a number game! Guess my number.");

        input = scan.nextInt(); //Scans user's guess

        number = r.nextInt(99) + 1; //provides a random number


        while (input != number) //Keep asking the user for a number
        {
            if (number < input)
                System.out.println("Too high! Try again!");
            else
                System.out.println("Too low! Try again!");

            input = scan.nextInt();
            tries++;//Keeps track of the number of tries

        }

        System.out.println("Yay! You got it right!");
        System.out.println(" And it only took " + tries +" tries!");
    }

}

