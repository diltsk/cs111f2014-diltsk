//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Practical#8
// Date: 11/20/14
//
// Purpose: TO play an annoying song
//****************************************************************

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import org.jfugue.Note;
import org.jfugue.Pattern;
import org.jfugue.PatternTransformer;
import org.jfugue.Player;
import org.jfugue.extras.ReversePatternTransformer;

public class FrereJacques
{
    public static void main(String[] args)
    {

        Scanner scan = new Scanner(System.in);
        int repeat;
        int var;
        var = 0;

        // "Frere Jacques"
        Pattern pattern1 = new Pattern("C5q D5q E5q C5q"); //creates a pattern

        // "Dormez-vous?"
        Pattern pattern2 = new Pattern("E5q F5q G5h"); //creates a pattern

        // "Sonnez les matines"
        Pattern pattern3 = new Pattern("G5i A5i G5i F5i E5q C5q"); //creates a pattern

        // "Ding ding dong"
        Pattern pattern4 = new Pattern("C5q G4q C5h"); // creates a pattern

        System.out.println("How many times would you like the song to play?");
        repeat = scan.nextInt();

        for (repeat = repeat; repeat > var; repeat--) { //makes the song repeat

        // Put all of the patters together to form the song
        Pattern song = new Pattern();
        song.add(pattern1, 2); // Adds 'pattern1' to 'song' twice
        song.add(pattern2, 2); // Adds 'pattern2' to 'song' twice
        song.add(pattern3, 2); // Adds 'pattern3' to 'song' twice
        song.add(pattern4, 2); // Adds 'pattern4' to 'song' twice


        // Play the song!
        Player player = new Player(); player.play(song);

        try {
            player.saveMidi(song, new File("frerejacues.mid"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        player.close();
        }

    }

}





