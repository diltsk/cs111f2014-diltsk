Kyle Dilts
Professor Kapfhammer
Computer Science 111
2 October 2014

Q: Do you recognize the names of any of the gvim plugins as they are being installed?
A: Yes, Amazing colors which is both solarized and and spf13 color pack.
Q: What do they do?
A. Allow the user to customize the color of java keywords to make the process of finding and
identify functions and errors easier.
Q: Please carefully study the new design of gvim-what are five ways in which it is now different from the
“default” configuration that you used previously?
A:  1. Different background, 2.. Different color scheme, 3. Shows % of program written depending on
where the user places their cursor, 4. Removes standard libre-style ribbon at the top of the page,
      5. Shows class pertaining to the line written at the bottom ribbon on the screen.
Q: How does your chosen plugin work?
A: NerdTree allows the user to search for files while using GVIM.
Q: What features does it provide?
A: An easier process of navigation through GVIM rather than through the terminal.
Q: Do you plan to use this plugin on a regular basis? Why or why not?
A: Most likely not, I find it simpler just to navigate using the terminal.
Q: What do you think the “gg=G” command does?
A:  gg=G highlights two closest curly brackets around a section of program.
Q: How do these commands allow you to manipulate your Java program?
A: They allow for easier navigation and deletion, thus speeding up the programming process; especially when
dealing with very large programs

