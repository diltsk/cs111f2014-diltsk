//*****************************
// Kyle Dilts
// CMPSC 111
// Practical 6
// October 29, 2014
//
// Purpose: a program that determines what activities happen
// during a specific year.
//*****************************

import java.util.Date;
import java.util.Scanner;
public class Practical6Main
{
    public static void main ( String[] args)
    {
        //Variable dictionary
        Scanner scan = new Scanner(System.in);
        int userInput;

        System.out.println("Please enter a year between 1000 and 3000!");
        userInput = scan.nextInt();

        YearChecker activities = new YearChecker(userInput);
        {
        activities.isLeapYear();
        if (isLeapYear() == true)
        System.out.println("This year is a leap year.");
        else
        System.out.println("This year is not a leap year.");
        }
        {
        activities.isCicadaYear();
        if (isCicadaYear() == true)
        System.out.println("This year is a cicada year.");
        else
        System.out.println("This year is not a cicada year.");
        }
        {
        activities.isSunspotYear();
        if (isSunspotYear() == true)
        System.out.println("This Year is a peak sunspot year.");
        else
        System.out.println("This year is not a peak sunspot year.");
        }

        System.out.println("Thank you for using this program.");
    }
}
