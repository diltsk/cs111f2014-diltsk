//****************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kyle Dilts
// CMPSC 111 Fall 2014
// Practical #03
// Date: 18 Sept. 2014
//
// Purpose: To make an entertaining MadLib.
//****************************************************************
import java.util.Date; //Needed for today's date
import java.util.Scanner; //Needed to use Scanner feature
public class Madlib 
{
    //-------------------------------------------
    // main method: program execution begins here
    //-------------------------------------------
    public static void main(String[] args)
    {
       // Label output with name and date:
       System.out.println("Kyle Dilts\nLab #\n" + new Date() + "\n");

       // Variable dictionary:
       // Madlib input variables
        String Firstname;		
	String DifFirstname;
	String Verb1;
	String Verb2;
	String SingularNoun1;
	String SingularNoun2;

       // Prompt user to enter data for Madlib fill-ins
	String message;
	Scanner scan = new Scanner(System.in);	

	System.out.print("Enter a first name: ");
	Firstname = scan.next();

	System.out.print("Enter a different first name: ");
	DifFirstname = scan.next();

	System.out.print("Enter a singular action: ");
	Verb1 = scan.next();

	System.out.print("Enter a different singular action: ");
	Verb2 = scan.next();

	System.out.print("Enter a random object or animal: ");
	SingularNoun1 = scan.next();

	System.out.print("Enter another random object or animal: ");
	SingularNoun2 = scan.next();

       //Print MadLib with fill-ins
	System.out.println("O " + Firstname + ", " + Firstname);
	System.out.println("Where for art thou " + Firstname + "?");
	System.out.println(Verb1 + " thy father and " + Verb2 + " thy name,");
	System.out.println("Or, thou wilt not, be but swom my " + SingularNoun1);
	System.out.println("And I'll no longer be a " + SingularNoun2);

    }
}
