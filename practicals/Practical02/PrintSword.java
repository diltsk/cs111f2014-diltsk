//*************************************
// Kyle Dilts
// Practical02, 11-12 September 2014
//
// Prints a sword
//*************************************
import java.util.Date;
public class PrintSword
{
 public static void main(String[] args)
 
 {
   System.out.println("Kyle Dilts\n" + new Date());
   System.out.println("                    /");
   System.out.println("	           |----------------------\\");
   System.out.println("/ - \\--------------|   	                   \\");
   System.out.println("0    ]		 | | –-----------------     >");
   System.out.println("\\ _ /--------------|   	                   /");
   System.out.println(" 		   |----------------------/");
   System.out.println("	            \\");
  }
}
